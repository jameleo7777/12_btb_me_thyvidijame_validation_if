
import './App.css';
import Validation_If_Else from './components/Validation_If_Else';

function App() {
  return (
    <div className="App">
      <Validation_If_Else/>
    </div>
  );
}

export default App;
