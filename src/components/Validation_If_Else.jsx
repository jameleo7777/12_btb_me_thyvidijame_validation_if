import React, { Component } from 'react'

export default class Validation_If_Else extends Component {
    constructor() {
        super();
        this.state = {
          Object:[],
          username:"",
          password:"",
          usernameValidate: false,
          passwordValidate:false,
          usernameError:"",
          passwordError:""
        };
       }

       inputChange = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        })
       }

       SubmitForm = (e) =>{
        e.preventDefault();
        let username = this.state.username
        let password = this.state.password
        let usernameValidate = true
        let passwordValidate = true
        let usernameError = ""
        let passwordError = ""

        let patternName  = /^[a-z|A-Z]{5,15}/
        let passwordPattern = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,10}$/

        if(username === ""){
            usernameError = "Please Input your name !."
            usernameValidate = false
        }else if(username.match(patternName) != null){
            usernameError = ""
            usernameValidate = true
        }else if(username.match(patternName) < 5){
            usernameError = "Must be more than 5 characters"
            usernameValidate = false
        }else if(username.match(patternName) > 15){
            usernameError = "Must be less than 15 characters"
            usernameValidate = false
        }

        if(password === "" ){
            passwordError = "Please Input your password !."
            passwordValidate = false
            
        }
        else if(password.match(passwordPattern) < 6){
            passwordError = "Must be more than 6 characters with digit and special symbol"
            passwordValidate = false
        }else if(password.match(passwordPattern) > 10){
            passwordError = "Must be less than 10 characters with digit and special symbol"
            passwordValidate = false
        }else if(password.match(passwordPattern) > 6 && password.match(passwordPattern) < 10 ){
            passwordError = ""
            passwordValidate = true
        }
        let newObject={
            username:this.state.username,
            password:this.state.password
         }

        this.setState({
            Object:[...this.state.Object, newObject],
            usernameError,
            passwordError,
            usernameValidate,
            passwordValidate,
        })

        if(usernameValidate && passwordValidate){
            alert("Success")
        }else{
            alert("Error")
        }

       }


  render() {
    return (
      <div>
           <form onSubmit={this.SubmitForm}>
           <div class="text-5xl text-center pt-10 font-bold ">
          <h1 class="bg-gradient-to-r from-blue-600 via-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">
            FORM LOGIN
          </h1>
        </div>
            <div class=" w-[70%] m-auto mt-4 ">
              <label
                for="input-group-1"
                class="block mb-2 font-medium text-gray-900 dark:text-black text-xl"
              >
                Username
              </label>
              <div class="relative mb-6">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none text-gray-600 font-bold">
                  😃
                </div>
                <input
                  type="text"
                  name="username"
                  onChange={this.inputChange}
                  class=" text-gray-900 text-sm rounded-md  block w-full pl-10 p-4  dark:bg-white dark:border-gray-600 dark:placeholder-black dark:text-black font-semibold"
                  placeholder="Enter your name"

                  
                />
              </div>
              <h1>{this.state.usernameError}</h1><br />
              
              

              <label
                for="input-group-1"
                class="block mb-2 font-medium text-gray-900 dark:text-black text-xl"
              >
                Password
              </label>
              <div className="relative mb-6">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  🔒
                </div>
                <input
                  type="password"
                  name="password"
                  onChange={this.inputChange}
                  className=" text-gray-900 text-sm rounded-md  block w-full pl-10 p-4  dark:bg-white dark:border-gray-600 dark:placeholder-black dark:text-black font-semibold"
                  placeholder="Enter your password"
                />
              </div>
              <h1>{this.state.passwordError}</h1><br />
              
              

              <div className="text-center">
                <button
                  type="submit"
                  class="  p-3 mt-6 w-[250px] rounded-md font-bold text-center text-white bg-blue-500 border-2 border-blue-500 "
                >
                  Submit
                </button>
              </div>
            </div>
          </form>

          <table class="mt-10 w-[70%] text-xl text-left text-black dark:text-black m-auto p-4">
        <thead class="text-md text-black dark:bg-slate-500 dark:text-white bg-gradient-to-r from-blue-600 via-green-500 to-indigo-400">
            <tr>
                <th scope="col" class="p-5 text-center rounded-l-md ">
                    Username
                </th>
                <th scope="col" class="p-5 text-center rounded-r-md">
                    Password
                </th>
            </tr>
        </thead>
        <tbody>
                {this.state.Object.map((items)=> 
                (
                    <tr  class="p-10 h-16 text-md text-center font-semibold text-black bg-slate-200" key={items}  >
                        <td class="rounded-l-md">{items.username === "" ? `null`:items.username}</td>
                        <td className="rounded-r-md">{items.password === ""? `null`:items.password}</td>
                        
                    </tr>
                   
                ))}
        </tbody>
    </table>
      </div>
    )
  }
}
